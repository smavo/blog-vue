import Vue from 'vue'
import App from './App.vue'

import VueRouter from 'vue-router';
import Vuelidate from 'vuelidate';

//Componentes Importados
import Home from './components/Home.vue';
import Blog from './components/Blog.vue';
import Hello from './components/HelloWorld.vue';
import Error404 from './components/Error404.vue';
import Search from './components/Search.vue';
import Redirect from './components/Redirect.vue';
import Article from './components/Article.vue';
import CreateArticle from './components/CreateArticle.vue';
import EditArticle from './components/EditArticle.vue';

Vue.config.productionTip = false

Vue.use(VueRouter);
Vue.use(Vuelidate);

const moment = require('moment');
require('moment/locale/es');
Vue.use(require('vue-moment'), {
    moment
});

const routes = [
    { path: '/', component: Home },
    { path: '/home', component: Home },
    { path: '/blog', component: Blog },
    { path: '/hello', component: Hello },
    { path: '/buscador/:searchString', component: Search },
    { path: '/redirect/:searchString', component: Redirect},
    { path: '/articulo/:id', name: 'article', component: Article},
    { path: '/crear-articulo', name: 'create', component: CreateArticle},
    { path: '/editar/:id', name: 'edit', component: EditArticle},
    { path: '/*', component: Error404 },
];

const router = new VueRouter({
    routes,
    mode: 'history'
});

new Vue({
    router,
    render: h => h(App),
}).$mount('#app')